import http from 'http';
import express from 'express';
import socketio from 'socket.io';

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const port = process.env.PORT || 8080;

enum MessageType {
    log = 'log',
    message = 'message',
    createOrJoin = 'create-or-join',
    created = 'created',
    joined = 'joined',
    remotePeerJoining = 'remote-peer-joining',
    broadcastJoined = 'broadcast-joined',
    full = 'full',
    response = 'response',
    bye = 'bye',
    ack = 'ack',
    gotUserMedia = 'got-user-media'
}

interface Message {
    channel: string;
    message: any;
}

io.on('connection', (socket) => {
    
    const sendLog = (logMessage: string) => {
        socket.emit(MessageType.log, `-->>> ${logMessage}`);
    };

    // Handle 'message' messages
    socket.on(MessageType.message, (payload: Message) => {
        sendLog(`S --> Got message: ${JSON.stringify(payload)}`);

        const { channel, message } = payload;
        socket.broadcast.to(channel).emit(MessageType.message, message);
    });

    // Handle 'create or join' messages
    socket.on(MessageType.createOrJoin, (payload: string) => {
        const room = io.nsps['/'].adapter.rooms[payload];
        const numClients = room ? room.length : 0;
        
        console.log(`Current num clients: ${numClients}, in room: '${payload}'`);

        // First client joining...
        if (numClients == 0) {
            socket.join(payload);
            socket.emit(MessageType.created, payload);
        } else if (numClients == 1) { // Second client joining...
            // Inform initiator...
            socket.broadcast.to(payload).emit(MessageType.remotePeerJoining, payload);

            // Let the new peer join channel
            socket.join(payload);

            socket.emit(MessageType.joined, `Joined channel: '${payload}'`);
            socket.broadcast.to(payload)
                .emit(MessageType.broadcastJoined, `S --> broadcast(): client ${socket.id} joined channel '${payload}'`);
        } else { // max 2 clients
            console.log('Channel full!');
            socket.emit(MessageType.full, payload);
        }
    });

    // Handle 'response' messages
    socket.on(MessageType.response, (payload: Message) => {
        sendLog(`S --> Got response: ${payload}`);

        // Just forward message to the other peer
        const { channel, message } = payload;
        socket.broadcast.to(channel).emit(MessageType.response, message);
    });

    // Handle 'Bye' messages
    socket.on(MessageType.bye, (payload: string) => {
        // Notify other peer
        socket.broadcast.to(payload).emit(MessageType.bye);

        // Close socket from server's side
        socket.disconnect();
    });

    // Handle 'Ack' messages
    socket.on(MessageType.ack, () => {
        console.log('Got an Ack!');
        // Close socket from server's side
        socket.disconnect();
    });

});

server.listen(port, () => {
    console.log(`Server started on port: ${port}`);
});
