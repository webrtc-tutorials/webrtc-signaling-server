FROM node:dubnium-slim

# Create app directory
WORKDIR /usr/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Bundle app source
COPY . .

RUN npm install && npm run build

EXPOSE 8080

CMD [ "node", "dist/bundle.js" ]